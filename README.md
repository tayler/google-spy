# Googley Eyes

## Why
Google’s mission is "to organize the world’s information and make it
universally accessible and useful". The information they are organizing and
making universally accessible and useful includes information about you and me.
To do so, the information has to first be collected. This is done through
several means. One is by attracting users to its useful and free services like
Google search and GMail. Keeping track of users' search queries and email
contents provides a lot of information to Google about individuals.
But it still leaves gaps. Google doesn't necessarily know what you do after you
leave its search page or read an email.

Another way Google collects this information is by offering a high-quality web browser (Google Chrome) and a high-quality internet service (Google Fiber). There are far fewer gaps in the information you make available to Google if you use either of these Google products. That's because, unlike Google search and GMail, both products know every site you visit.

There is a third way that Google can collect information about you, but it doesn't require your action or consent. Google offers a number of extremely useful services to companies and individuals who develop the web sites you visit. Developers make use of these services on their sites because they provide useful functionality usually for free. In return, Google can fill in the gaps in its information about you and me. These services let Google know where we are when we are not on one of its sites or using one of its services intentionally. They fill in those gaps in Google's information mentioned above. This extension shows you what Google is finding out about you even when you are not directly using one of their services like GMail. It also brings to light the non-Google sites that enable Google to do so.

Copyright 2016 Tayler

## License (1.0.6+)
This file is part of Googley Eyes.

Googley Eyes is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Googley Eyes is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See the accompanying COPYING file for more details.