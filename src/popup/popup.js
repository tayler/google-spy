/* eslint-env webextensions */

var googleyEyes = googleyEyes();
var openStats = document.getElementById('OpenStats');
openStats.addEventListener('click', function() {
    chrome.tabs.create({
        url: '../stats-tab/stats.html',
        active: true,
    });
});

chrome.storage.local.get('hosts', function(storage) {
    if (chrome.runtime.lastError) {
        console.error(chrome.runtime.lastError);
    }
    else {
        if (storage.hosts) {
            googleyEyes.createPieChart(storage.hosts, {chartTitle: false});
        }
        else {
            // show #NoStats
            document.getElementById('NoStats').classList.remove('hide');
            document.getElementById('PieChart').classList.add('hide');
        }
    }
});
