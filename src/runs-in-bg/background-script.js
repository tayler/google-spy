/* eslint-env webextensions */

function isEmpty(obj) {
   for (var x in obj) { if (obj.hasOwnProperty(x))  return false; }
   return true;
}

// listen for new messages and save the newly-added hosts
chrome.runtime.onMessage.addListener(function(message) {
    chrome.storage.local.get('hosts', function(storage) {
        if (chrome.runtime.lastError) {
            console.error(chrome.runtime.lastError);
        }
        else {
            if (isEmpty(storage)) {
                // this would/should only happen if first time
                storage.hosts = [message];
            }
            else {
                storage.hosts.push(message);
            }
            // add this one to the list of previous hosts and save
            chrome.storage.local.set({hosts: storage.hosts});
            // change popup button to red eye if google present
            if (message.uses_google) {
                // get current tab's id, so it only changes for this tab
                chrome.tabs.query({
                    currentWindow: true,
                    active: true,
                }, (currentTabs) => {
                    chrome.browserAction.setIcon({path: '../../icons/red-eye.svg', tabId: currentTabs[0].id});
                    chrome.browserAction.setTitle({title: 'This page is sending information to Google', tabId: currentTabs[0].id});
                });
            }
            else {
                chrome.tabs.query({
                    currentWindow: true,
                    active: true,
                }, (currentTabs) => {
                    // reset to default icon and title
                    chrome.browserAction.setIcon({path: '../../icons/g-eye.svg', tabId: currentTabs[0].id});
                    chrome.browserAction.setTitle({title: 'Googley Eyes', tabId: currentTabs[0].id});
                });
            }
        }
    });
});