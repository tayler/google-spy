/* eslint-env webextensions */

var googleyEyes = googleyEyes();

function fillHostTotals(counts) {
    var hasTracking = document.getElementById('TrackingHostsTotal');
    var noTracking = document.getElementById('NoTrackingHostsTotal');

    hasTracking.innerHTML = counts.hasGooglePresence.length;
    noTracking.innerHTML = counts.noGooglePresence.length;
}

function fillPresenceLists(counts) {
    var hasPresenceList = document.getElementById('HasPresenceList');
    var noPresenceList = document.getElementById('NoPresenceList');
    // have to get just the host name before we can filter to get just uniques
    var hostsWithGoogle = counts.hasGooglePresence.map((host) => host.name.replace('www.', ''));
    var hostsWithoutGoogle = counts.noGooglePresence.map((host) => host.name.replace('www.', ''));
    var uniqueHasGoogle = googleyEyes.uniq(hostsWithGoogle).sort();
    var uniqueNoGoogle = googleyEyes.uniq(hostsWithoutGoogle).sort();
    
    uniqueHasGoogle.forEach((host) => {
        let listEl = document.createElement('div');
        listEl.classList.add('list-item');
        let content = document.createTextNode(host);
        listEl.appendChild(content);
        hasPresenceList.appendChild(listEl);
    });
    
    uniqueNoGoogle.forEach((host) => {
        let listEl = document.createElement('div');
        listEl.classList.add('list-item');
        let content = document.createTextNode(host);
        listEl.appendChild(content);
        noPresenceList.appendChild(listEl);
    });
}

var clearStats = document.getElementById('ClearStats');

chrome.storage.local.get('hosts', function(storage) {
    if (chrome.runtime.lastError) {
        console.error(chrome.runtime.lastError);
    }
    else {
        if (storage.hosts) {
            var counts = googleyEyes.getPresenceCounts(storage.hosts);
            googleyEyes.createPieChart(storage.hosts, {chartTitle: true});
            fillPresenceLists(counts);
            fillHostTotals(counts);
            clearStats.addEventListener('click', () => {
                if (window.confirm("Clear all data?")) {
                    chrome.storage.local.remove('hosts', () => {
                        window.location.reload();
                    });
                }
            });
        }
        else {
            clearStats.classList.add('disabled-btn');
            // show #NoStats
            document.getElementById('NoStats').classList.remove('hide');
        }
    }
});
