/*eslint-env webextensions*/

// will match google-analytics.com, googleapis.com, fonts.googleapis.com
var googlePresenceRegexes = [/google/, /gstatic/, /doubleclick/, /youtube/, /ytimg/];
// get all the scripts on the page
var scriptsCollection = document.getElementsByTagName('script');
// make it an array so we can use array methods
var scripts = [...scriptsCollection];

var siteUsesGoogle = scripts.some((script) => {
    if (script.src.length) {
        // test this script against each regex in `googlePresenceRegexes`
        /// until it finds the first match
        let googleScriptPresent = googlePresenceRegexes.some((regex) => {
            return regex.test(script.src);
        });

        // this finishes the first time googleScriptPresent is `true`
        return googleScriptPresent;
    }
});
chrome.runtime.sendMessage({'uses_google': siteUsesGoogle, 'name': document.location.host});
