/* eslint-env webextensions */
/* global nv d3 */

function googleyEyes() {
    var presenceCounts = {};

    function isEmpty(obj) {
        for (var x in obj) { if (obj.hasOwnProperty(x))  return false; }
        return true;
    }

    function uniq(a) {
        return Array.from(new Set(a));
    }

    function getPresenceCounts(hosts) {
        if (!isEmpty(presenceCounts)) {
            return presenceCounts;
        }
        var hasGooglePresence = [];
        var noGooglePresence = [];

        hosts.forEach((host) => {
            if (host.uses_google) {
                hasGooglePresence.push(host);
            }
            else {
                noGooglePresence.push(host);
            }
        });

        presenceCounts = {hasGooglePresence, noGooglePresence};

        return presenceCounts;
    }

    function getPieChartData(presenceCounts) {
        var hasPresenceCount = presenceCounts.hasGooglePresence.length;
        var noPresenceCount = presenceCounts.noGooglePresence.length;
        var hostCount = hasPresenceCount + noPresenceCount;

        return [
            {
                label: 'Google Present',
                value: hasPresenceCount / hostCount,
            },
            {
                label: 'Google Not Present',
                value: noPresenceCount / hostCount,
            },
        ];
    }

    function createPieChart(hosts, options) {
        var counts = getPresenceCounts(hosts);
        var totalSiteCount = counts.hasGooglePresence.length + counts.noGooglePresence.length;
        var percentages = getPieChartData(counts);
        var title = (options.chartTitle) ? `${totalSiteCount} Pages Visited` : false;
        var padAngle = (percentages[0].value == 0 || percentages[0].value == 1.0) ? 0 : 0.05;

        // nvd3 docs at http://nvd3-community.github.io/nvd3/examples/documentation.html#pieChart
        nv.addGraph(function() {
            var chart = nv.models.pieChart()
            .x(function(d) { return d.label })
            .y(function(d) { return d.value })
            .showLabels(true)     //Display pie labels
            .labelThreshold(.05)  //Configure the minimum slice size for labels to show up
            .labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
            .donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
            .donutRatio(0.35)     //Configure how big you want the donut hole size to be.
            .color(['#FF5722', '#31A9B8'])
            .padAngle(padAngle)
            .title(title);

            d3.select("#PieChart")
            .datum(percentages)
            .transition().duration(350)
            .call(chart);

            return chart;
        });
    }

    return {
        uniq,
        getPresenceCounts,
        createPieChart,
    }
}


